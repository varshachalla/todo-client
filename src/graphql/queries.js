import gql from 'graphql-tag';

export const TODO_ITEMS = gql`
  query todoItems {
    todoItems {
      id
      body
    }
  }
`;

export const ADD_TODO_ITEM = gql`
  mutation addTodoItem($body: String!) {
    addTodoItem(body: $body) {
      id
      body
    }
  }
`

export const EDIT_TODO_ITEM = gql`
  mutation updateTodoItem($id: ID!, $body: String!) {
    updateTodoItem(id: $id, body: $body) {
     id
     body
    }
  }
`

export const DELETE_TODO_ITEM = gql`
  mutation deleteTodoItem($id: ID!) {
    deleteTodoItem(id: $id) {
      id
      body
    }
  }
`