import ApolloClient from 'apollo-boost';
import { HttpLink } from 'apollo-link-http';
import React from 'react';
import { ApolloProvider } from 'react-apollo';
import ReactDOM from 'react-dom';

import Layout from './components/Layout';

const client = new ApolloClient({
    link: new HttpLink()
});

ReactDOM.render(<ApolloProvider client={client}><Layout /></ApolloProvider>, document.getElementById('root'));

