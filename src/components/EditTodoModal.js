import {
  Button,
  Modal,
  TextField,
  Typography,
  withStyles
} from "@material-ui/core";
import PropTypes from "prop-types";
import React from "react";
import { Mutation } from "react-apollo";

import { EDIT_TODO_ITEM } from "../graphql/queries";

const styles = theme => ({
  paper: {
    position: "absolute",
    width: theme.spacing.unit * 25,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    top: "35%",
    left: "35%"
  }
});

class AddTodoModal extends React.Component {
  constructor() {
    super();
    this.state = {
      formData: {}
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    var targetId = event.target.id;
    var targetValue = event.target.value;
    this.setState(prevState => ({
      formData: {
        ...prevState.formData,
        [targetId]: targetValue
      }
    }));
  }

  componentWillReceiveProps(props) {
    this.setState({ formData: props.todoItem });
  }

  render() {
    const { classes, onClose, open } = this.props;
    const {
      formData: { body, id }
    } = this.state;
    return (
      <div>
        <Modal open={open} onClose={onClose}>
          <div className={classes.paper}>
            <Typography variant="title" id="modal-title">
              Edit Todo
            </Typography>
            <TextField
              id="body"
              label="todo"
              type="text"
              onChange={this.handleChange}
              margin="normal"
              value={this.state.formData.body}
            />
            <br />
            <Mutation mutation={EDIT_TODO_ITEM} variables={{ id, body }}>
              {updateTodoItem => (
                <Button
                  className={classes.button}
                  color="primary"
                  onClick={() => {
                    updateTodoItem();
                    this.props.onClose();
                  }}
                >
                  Save
                </Button>
              )}
            </Mutation>
          </div>
        </Modal>
      </div>
    );
  }
}

AddTodoModal.propTypes = {
  classes: PropTypes.object.isRequired,
  open: PropTypes.string.isRequired,
  onClose: PropTypes.func.isRequired
};

export default withStyles(styles)(AddTodoModal);
