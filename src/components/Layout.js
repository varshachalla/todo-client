import { Button } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import React, { Component, Fragment } from "react";

import AddTodoModal from "./AddTodoModal";
import TodoItems from "./TodoItems";

class Layout extends Component {
  constructor() {
    super();
    this.state = {
      addTodoOpen: false
    };
  }
  render() {
    const { addTodoOpen } = this.state;
    return (
      <Fragment>
        <Button
          mini
          text-align="right"
          variant="fab"
          aria-label="Add"
          color="primary"
          onClick={() => {
            this.setState({ addTodoOpen: true });
          }}
        >
          <AddIcon />
        </Button>
        <AddTodoModal
          open={addTodoOpen}
          onClose={() => {
            this.setState({ addTodoOpen: false });
          }}
        />
        <TodoItems />
      </Fragment>
    );
  }
}

export default Layout;
