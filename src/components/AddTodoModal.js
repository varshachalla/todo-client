import {
  Button,
  Modal,
  TextField,
  Typography,
  withStyles
} from "@material-ui/core";
import PropTypes from "prop-types";
import React from "react";
import { Mutation } from "react-apollo";

import { ADD_TODO_ITEM, TODO_ITEMS } from "../graphql/queries";

const styles = theme => ({
  paper: {
    position: "absolute",
    width: theme.spacing.unit * 25,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    top: "35%",
    left: "35%"
  }
});

class AddTodoModal extends React.Component {
  constructor() {
    super();
    this.state = {
      formData: {}
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    var targetId = event.target.id;
    var targetValue = event.target.value;
    this.setState(prevState => ({
      formData: {
        ...prevState.formData,
        [targetId]: targetValue
      }
    }));
  }

  render() {
    const { classes, onClose, open } = this.props;
    const { body } = this.state.formData;
    return (
      <div>
        <Modal open={open} onClose={onClose}>
          <div className={classes.paper}>
            <Typography variant="title" id="modal-title">
              Add Todo
            </Typography>
            <TextField
              id="body"
              label="body"
              type="text"
              onChange={this.handleChange}
              margin="normal"
            />
            <br />
            <Mutation mutation={ADD_TODO_ITEM} variables={{ body }}>
              {addTodoItem => (
                <Button
                  className={classes.button}
                  color="primary"
                  onClick={() => {
                    addTodoItem({
                      refetchQueries: [
                        {
                          query: TODO_ITEMS
                        }
                      ]
                    });
                    this.setState({ body: "" });
                    this.props.onClose();
                  }}
                >
                  Add
                </Button>
              )}
            </Mutation>
          </div>
        </Modal>
      </div>
    );
  }
}

AddTodoModal.propTypes = {
  classes: PropTypes.object.isRequired,
  open: PropTypes.string.isRequired,
  onClose: PropTypes.func.isRequired
};

export default withStyles(styles)(AddTodoModal);
