import {
  Button,
  Card,
  CardActions,
  CardContent,
  Typography,
  withStyles
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import PropTypes from "prop-types";
import React, { Fragment, Component } from "react";
import { Mutation } from "react-apollo";

import EditTodoModal from "./EditTodoModal";
import { TODO_ITEMS, DELETE_TODO_ITEM } from "../graphql/queries";

const styles = {
  card: {
    width: 200,
    marginTop: 30
  },
  bullet: {
    display: "inline-block",
    marginTop: 0,
    marginRight: 2,
    transform: "scale(0.8)"
  }
};

class TodoItem extends Component {
  constructor() {
    super();
    this.state = {
      editTodoOpen: false
    };
  }

  render() {
    const { classes, body, id } = this.props;
    const { editTodoOpen } = this.state;
    const bull = <span className={classes.bullet}>•</span>;
    return (
      <Fragment>
        <Card className={classes.card}>
          <CardContent>
            <Typography variant="h6" component="h2">
              {bull}
              {body}
            </Typography>
          </CardContent>
          <CardActions>
            <Button
              size="small"
              onClick={() => {
                this.setState({ editTodoOpen: true });
              }}
            >
              <EditIcon />
            </Button>
            <Mutation mutation={DELETE_TODO_ITEM} variables={{ id }}>
              {deleteTodoItem => (
                <Button
                  className={classes.button}
                  size="small"
                  onClick={() =>
                    deleteTodoItem({
                      refetchQueries: [
                        {
                          query: TODO_ITEMS
                        }
                      ]
                    })
                  }
                >
                  <DeleteIcon />
                </Button>
              )}
            </Mutation>
          </CardActions>
        </Card>
        <EditTodoModal
          todoItem={{ body, id }}
          open={editTodoOpen}
          onClose={() => {
            this.setState({ editTodoOpen: false });
          }}
        />
      </Fragment>
    );
  }
}

TodoItem.propTypes = {
  classes: PropTypes.object.isRequired,
  body: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired
};

export default withStyles(styles)(TodoItem);
