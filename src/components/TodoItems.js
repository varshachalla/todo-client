import { CircularProgress } from "@material-ui/core";
import React, { Component, Fragment } from "react";
import { Query } from "react-apollo";

import { TODO_ITEMS } from "../graphql/queries";
import TodoItem from "./TodoItem";

class TodoItems extends Component {
  render() {
    return (
      <Query query={TODO_ITEMS}>
        {({ loading, error, data }) => {
          if (loading) return <CircularProgress />;
          if (error) return `Error! ${error.message}`;
          return (
            <Fragment>
              {data.todoItems.map(item => (
                <TodoItem id={item.id} body={item.body} />
              ))}
            </Fragment>
          );
        }}
      </Query>
    );
  }
}

export default TodoItems;
